# Raspberry pi Docker based Media Server

This docker-compose file will create containers of the the following software
- [Emby](https://emby.media/)
- [Sonarr](https://sonarr.tv/)
- [Radarr](https://radarr.video/)
- [Transmission](https://transmissionbt.com/)
- [Jackett](https://github.com/Jackett/Jackett)

## Preparations
To be able to run those docker containers you will need to install **docker** and **docker-compose** 

Installing **docker**

```
curl -sSL https://get.docker.com | sh
```

Add permission to user pi to run docker commands

```
sudo usermod -aG docker pi
```
Reboot for changes to be applied
```
sudo reboot
```

Install **docker-compose** dependencies
```
sudo apt install -y libffi-dev libssl-dev python3 python3-pip
```

Install **docker-compose**
```
sudo pip3 install docker-compose
```

## Put **Media Server** up

Once you installed **docker** and **docker-compose** you'll be able to put all the necessary containers up by navigating to this project root folder and calling:
```
docker-compose up -d
```


